//
//  DetailCoordinator.swift
//  Coordinator Example
//
//  Created by Leonardo Amaral Sousa on 12/08/20.
//  Copyright © 2020 LAS Solucoes. All rights reserved.
//

import UIKit


class DetailCoordinator: Coordinator, LoginCoordinatorDelegate{
    
    var isLoggedIn = false
    var user: String?
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    weak var parentCoordinator: MainCoordinator?
    
    init(_ navigation: UINavigationController) {
        navigationController = navigation
    }
    
    func start() {
  
    }
    
    func start(_ user:String) {
        if isLoggedIn {
           let vc = DetailViewController()
           vc.coordinator = self
           vc.user = user
           vc.title = "DETAIL"
           navigationController.pushViewController(vc, animated: true)
        } else {
            self.user = user
            let loginCoordinator = LoginCoordinator(navigationController)
            loginCoordinator.parentCoordinator = self;
            loginCoordinator.delegate = self
            self.childCoordinators.append(loginCoordinator)
            loginCoordinator.start()
        }
    }
    
    func didFinishLogin() {
        self.isLoggedIn = true;
        start(user!)
    }
    
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
}
