//
//  DetailViewController.swift
//  Coordinator Example
//
//  Created by Leonardo Amaral Sousa on 12/08/20.
//  Copyright © 2020 LAS Solucoes. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {    
    let myView = DetailView()
    weak var coordinator: DetailCoordinator?
    var user: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = myView
        bindView()
    }
    
    func bindView() {
        myView.title.text = user!
    }
    

}
