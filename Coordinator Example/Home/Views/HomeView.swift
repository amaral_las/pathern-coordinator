//
//  HomeView.swift
//  Coordinator Example
//
//  Created by Leonardo Amaral Sousa on 12/08/20.
//  Copyright © 2020 LAS Solucoes. All rights reserved.
//
import UIKit

class HomeView: UIView {
    
    let table : UITableView = {
        var x = UITableView()
        x.allowsSelection = true
        x.register(StandardTableViewCell.self, forCellReuseIdentifier: "standardTableViewCell")
        
        return x
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView();
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    
    func setupView(){
        
        self.backgroundColor = .white
        self.addSubview(table)
        //tableViewHome Constraints
        table.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        table.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        table.topAnchor.constraint(equalTo: topAnchor).isActive = true
        table.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        table.separatorStyle = .none
        table.translatesAutoresizingMaskIntoConstraints = false
    }
    
    
}
