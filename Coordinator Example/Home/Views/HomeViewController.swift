//
//  HomeViewController.swift
//  Coordinator Example
//
//  Created by Leonardo Amaral Sousa on 12/08/20.
//  Copyright © 2020 LAS Solucoes. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    let users : [String] = ["Igor", "Belo", "Patricia", "Laercio", "Edno da Massa"]
    let myView = HomeView()
    
    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = myView
        
        myView.table.delegate = self
        myView.table.dataSource = self
    }

}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "standardTableViewCell") as! StandardTableViewCell
        cell.selectionStyle = .none
        cell.title.text = self.users[indexPath.row]
        cell.desc.text = "id \(indexPath.row): \(self.users[indexPath.row])"
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.coordinator?.toDetail(self.users[indexPath.row])
    }
    
}
