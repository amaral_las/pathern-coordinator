//
//  StandardTableViewCell.swift
//  Coordinator Example
//
//  Created by Leonardo Amaral Sousa on 12/08/20.
//  Copyright © 2020 LAS Solucoes. All rights reserved.
//

import UIKit

class StandardTableViewCell : UITableViewCell {
    let title : UILabel = UILabel()
    let desc = UILabel()
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
    }
    
    func setUpView(){
        let stackViewCell : UIStackView = UIStackView()
        stackViewCell.axis = .vertical
        stackViewCell.addArrangedSubview(title)
        stackViewCell.addArrangedSubview(desc)
        self.addSubview(stackViewCell)
        
        stackViewCell.topAnchor.constraint(equalTo: self.topAnchor, constant: 15).isActive = true
        stackViewCell.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        stackViewCell.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        stackViewCell.translatesAutoresizingMaskIntoConstraints = false
    }
}
