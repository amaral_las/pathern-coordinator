//
//  MainCoordinator.swift
//  Coordinator Example
//
//  Created by Leonardo Amaral Sousa on 12/08/20.
//  Copyright © 2020 LAS Solucoes. All rights reserved.
//

import UIKit

class MainCoordinator: NSObject, Coordinator{
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        navigationController.delegate = self
        let vc = HomeViewController()
        vc.coordinator = self
        vc.title = "HOME"
        navigationController.pushViewController(vc, animated: false)
    }
    
    func toDetail(_ user: String){
        let detailCoordinator = DetailCoordinator(navigationController)
        detailCoordinator.parentCoordinator = self;
        self.childCoordinators.append(detailCoordinator)
        detailCoordinator.start(user)
    }
    
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
}

extension MainCoordinator: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        // Read the view controller we’re moving from.
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }

        // Check whether our view controller array already contains that view controller. If it does it means we’re pushing a different view controller on top rather than popping it, so exit.
        if navigationController.viewControllers.contains(fromViewController) {
            return
        }
        
        if let detailViewController = fromViewController as? DetailViewController {
            // We're popping a buy view controller; end its coordinator
            childDidFinish(detailViewController.coordinator)
        }
    }
}
