//
//  LoginCoordinator.swift
//  Coordinator Example
//
//  Created by Leonardo Amaral Sousa on 12/08/20.
//  Copyright © 2020 LAS Solucoes. All rights reserved.
//
import UIKit

protocol LoginCoordinatorDelegate: AnyObject {
    func didFinishLogin()
}

class LoginCoordinator: Coordinator {
    
    weak var delegate: LoginCoordinatorDelegate?
    let vc = LoginViewController()
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    weak var parentCoordinator: DetailCoordinator?
    
    init(_ navigation: UINavigationController) {
        navigationController = navigation
    }
    
    func start() {
        vc.coordinator = self
        vc.title = "LOGIN"
        navigationController.pushViewController(vc, animated: true)
    }
    
    func finishLogin() {
        parentCoordinator?.childDidFinish(self)
        delegate!.didFinishLogin()
        var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
        navigationArray.remove(at: navigationArray.count - 2) // To remove previous UIViewController
        self.navigationController.viewControllers = navigationArray
    }
}
