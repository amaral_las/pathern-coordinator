//
//  LoginView.swift
//  Coordinator Example
//
//  Created by Leonardo Amaral Sousa on 12/08/20.
//  Copyright © 2020 LAS Solucoes. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    lazy var btn_submit: UIButton = {
        var x = UIButton()
        x.setTitle("Entrar", for: .normal)
        x.backgroundColor = .blue
        x.setTitleColor(.white, for: .normal)

        return x
    }()
    
    lazy var labelNome: UILabel = {
        var x = UILabel()
        x.text = "Usuário:"
        return x
    }()
    
    lazy var labelSenha: UILabel = {
        var x = UILabel()
        return x
    }()
    
    lazy var inputNome: UITextField = {
        var x = UITextField()
        x.borderStyle = .roundedRect
        x.autocapitalizationType = .none
        return x
    }()
    
    lazy var inputSenha: UITextField = {
        var x = UITextField()
        x.borderStyle = .roundedRect
        x.isSecureTextEntry = true
        /*Somente para parar de digitar e passar para proxima tela*/x.text = "a"
        return x
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView();
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupView(){
        self.backgroundColor = .white
        let stack = UIStackView()
        self.addSubview(stack)
        
    
        stack.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        stack.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        stack.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        stack.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        stack.axis = .vertical
        stack.distribution = .fill
        
        stack.addArrangedSubview(labelNome)
        stack.addArrangedSubview(inputNome)
        stack.setCustomSpacing(30, after: inputNome)
        
        stack.addArrangedSubview(labelSenha)
        stack.addArrangedSubview(inputSenha)
        stack.setCustomSpacing(30, after: inputSenha)
        stack.addArrangedSubview(btn_submit)
        
        
        stack.translatesAutoresizingMaskIntoConstraints = false
    }
    
}
