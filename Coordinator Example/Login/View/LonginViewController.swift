//
//  LonginViewController.swift
//  Coordinator Example
//
//  Created by Leonardo Amaral Sousa on 12/08/20.
//  Copyright © 2020 LAS Solucoes. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    let myView = LoginView()
    weak var coordinator: LoginCoordinator?
    
    var usr: String = ""
    var pass: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
       
        view = myView
    
        myView.btn_submit.addTarget(self, action: #selector(login), for: .touchUpInside)
        
    }
    
    
    @objc func login(){
        dismiss(animated: false)
        coordinator?.finishLogin()
    }

}
